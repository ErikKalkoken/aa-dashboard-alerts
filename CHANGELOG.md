# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.9.1] - 2020-09-24

### Changed

- Updated to work with Auth 2.8
- Added Django 3 to test suite

## [0.9.0] - 2020-08-07

### Changed

- Updated to work with standingsrequests 0.5.0
- Updated dashboard template for Alliance Auth 2.7.3

## [0.8.0] - 2020-06-17

### Changed

- Updated dashboard template for AA 2.7.2
- Migrated to Black formatting for codebase
- Dropped support for Python 3.5

## [0.7.9] - 2020-03-10

### Changed

- Updated dashboard template for Alliance Auth 2.6.2

## [0.7.8] - 2020-02-18

### Changed

- Updated dashboard template for Alliance Auth 2.6.0

## [0.7.7] - 2020-01-16

### Changed

- Updated dashboard template for Alliance Auth 2.3.0

## [0.7.6] - 2019-12-19

### Added

- Added pipeline testing on GitLab

## [0.7.5] - 2019-09-26

### Added

- Initial
