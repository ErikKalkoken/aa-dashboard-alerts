from django.conf.urls import url
from . import views

app_name = "dashboard_alerts"

urlpatterns = [
    url(r"^get_todo_counts/$", views.get_todo_counts, name="get_todo_counts"),
]
