import json
from unittest.mock import patch

from django.test import TestCase
from django.contrib.auth.models import User, Group, Permission
from django.test.client import RequestFactory
from django.utils.timezone import now

from allianceauth.groupmanagement.models import GroupRequest
from allianceauth.notifications import notify
from allianceauth.srp.models import SrpUserRequest, SrpFleetMain
from standingsrequests.models import StandingRequest, StandingRevocation

from ..views import get_todo_counts


# test path: dashboard_alerts.tests.TestDashboardAddOn
class TestDashboardAddOnView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        # user 1 is leader of group1 and a member
        self.user1 = User.objects.create_user("user1")
        self.group1 = Group.objects.create(name="group1")
        self.group1.authgroup.group_leaders.add(self.user1)
        self.user1.groups.add(self.group1)

        # user 2 is leader of group2 and a member
        self.user2 = User.objects.create_user("user2")
        self.group2 = Group.objects.create(name="group2")
        self.group2.authgroup.group_leaders.add(self.user2)
        self.user2.groups.add(self.group1)

        self.user3 = User.objects.create_user("user3")
        self.user4 = User.objects.create_user("user4")

        # we are using our own config
        # which will override the current config for all tests
        self.DASHBOARD_ADD_ON_CONFIG = {
            "notifications_enabled": True,
            "group_requests_enabled": True,
            "standings_requests_enabled": True,
            "srp_requests_enabled": True,
            "warning_threshold": 10,
            "danger_threshold": 25,
            "disable_app_alerts_for_superusers": False,
        }

    @patch("dashboard_alerts.views.get_config")
    def test_no_alerts(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertFalse(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_notifications_only(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        notify(self.user1, "dummy", "dummy")
        notify(self.user1, "dummy", "dummy")
        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 2)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_group_requests_only(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        GroupRequest.objects.create(
            status="pending", user=self.user3, group=self.group1
        )
        GroupRequest.objects.create(
            status="pending", user=self.user4, group=self.group1
        )
        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 2)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_group_requests_N_notifications(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        notify(self.user1, "dummy", "dummy")
        notify(self.user1, "dummy", "dummy")
        notify(self.user1, "dummy", "dummy")
        GroupRequest.objects.create(
            status="pending", user=self.user3, group=self.group1
        )
        GroupRequest.objects.create(
            status="pending", user=self.user4, group=self.group1
        )
        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 3)
        self.assertEqual(data["group_requests_count"], 2)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_group_requests_others(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        GroupRequest.objects.create(
            status="pending", user=self.user3, group=self.group2
        )
        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertFalse(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_group_requests_non_leader(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        GroupRequest.objects.create(
            status="pending", user=self.user3, group=self.group1
        )
        GroupRequest.objects.create(
            status="pending", user=self.user4, group=self.group1
        )
        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user3
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertFalse(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_standings_requests_user_has_perm(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        StandingRequest.objects.create(
            user=self.user3,
            contact_id=1,
            contact_type_id=1,
        )
        StandingRequest.objects.create(
            user=self.user3,
            contact_id=2,
            contact_type_id=1,
        )
        s = StandingRequest.objects.create(
            user=self.user4,
            contact_id=4,
            contact_type_id=1,
        )
        s.action_by = self.user1
        s.save()
        StandingRevocation.objects.create(
            contact_id=4,
            contact_type_id=1,
        )
        p = Permission.objects.get(
            codename="affect_standings", content_type__app_label="standingsrequests"
        )
        self.user1.user_permissions.add(p)
        User.objects.get(pk=self.user1.pk)

        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["standings_requests_count"], 3)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_standings_requests_user_no_perm(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        StandingRequest.objects.create(
            user=self.user3,
            contact_id=1,
            contact_type_id=1,
        )
        StandingRequest.objects.create(
            user=self.user3,
            contact_id=2,
            contact_type_id=1,
        )
        s = StandingRequest.objects.create(
            user=self.user4,
            contact_id=4,
            contact_type_id=1,
        )
        s.action_by = self.user1
        s.save()

        StandingRevocation.objects.create(
            contact_id=4,
            contact_type_id=1,
        )
        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1
        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertFalse(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["standings_requests_count"], 0)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_all_types(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        notify(self.user1, "dummy1", "dummy1")
        notify(self.user1, "dummy2", "dummy2")
        notify(self.user1, "dummy3", "dummy3")
        GroupRequest.objects.create(
            status="pending", user=self.user3, group=self.group1
        )
        GroupRequest.objects.create(
            status="pending", user=self.user4, group=self.group1
        )

        StandingRequest.objects.create(
            user=self.user3,
            contact_id=1,
            contact_type_id=1,
        )
        StandingRequest.objects.create(
            user=self.user3,
            contact_id=2,
            contact_type_id=1,
        )
        s = StandingRequest.objects.create(
            user=self.user4,
            contact_id=4,
            contact_type_id=1,
        )
        s.action_by = self.user1
        s.save()
        StandingRevocation.objects.create(
            contact_id=4,
            contact_type_id=1,
        )
        fleet = SrpFleetMain.objects.create(fleet_time=now())

        SrpUserRequest.objects.create(
            killboard_link="https://zkillboard.com/kill/79111612/",
            srp_status="Pending",
            srp_fleet_main=fleet,
        )
        p = Permission.objects.get(
            codename="srp_management", content_type__app_label="auth"
        )
        self.user1.user_permissions.add(p)
        p = Permission.objects.get(
            codename="affect_standings", content_type__app_label="standingsrequests"
        )
        self.user1.user_permissions.add(p)
        User.objects.get(pk=self.user1.pk)

        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1

        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 3)
        self.assertEqual(data["group_requests_count"], 2)
        self.assertEqual(data["standings_requests_count"], 3)
        # self.assertEqual(data['srp_requests_count'], 1)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_srp_requests(self, mock_get_config):
        mock_get_config.return_value = self.DASHBOARD_ADD_ON_CONFIG
        fleet = SrpFleetMain.objects.create(fleet_time=now())
        SrpUserRequest.objects.create(
            killboard_link="https://zkillboard.com/kill/79111612/",
            srp_status="Pending",
            srp_fleet_main=fleet,
        )
        p = Permission.objects.get(
            codename="srp_management", content_type__app_label="auth"
        )
        self.user1.user_permissions.add(p)
        User.objects.get(pk=self.user1.pk)

        request = self.factory.get("/dashboard-addon/get_todo_counts")
        request.user = self.user1

        data = json.loads(get_todo_counts(request).content.decode("utf-8"))
        # self.assertTrue(data['has_to_dos'])
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["standings_requests_count"], 0)
        # self.assertEqual(data['srp_requests_count'], 1)
        self.assertEqual(data["alert_level"], "info")


class TestDashboardAddOnConfig(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        # user 1 is leader of group1 and a member
        self.user1 = User.objects.create_user("user1")
        self.group1 = Group.objects.create(name="group1")
        self.group1.authgroup.group_leaders.add(self.user1)
        self.user1.groups.add(self.group1)

        # user 2 is leader of group2 and a member
        self.user2 = User.objects.create_user("user2")
        self.group2 = Group.objects.create(name="group2")
        self.group2.authgroup.group_leaders.add(self.user2)
        self.user2.groups.add(self.group1)

        self.user3 = User.objects.create_user("user3")
        self.user4 = User.objects.create_user("user4")

        # we are using our own config
        # which will override the current config for all tests
        self.DASHBOARD_ADD_ON_CONFIG = {
            "notifications_enabled": True,
            "group_requests_enabled": True,
            "standings_requests_enabled": True,
            "srp_requests_enabled": True,
            "warning_threshold": 10,
            "danger_threshold": 25,
            "disable_app_alerts_for_superusers": False,
        }
        notify(self.user1, "dummy1", "dummy1")
        notify(self.user1, "dummy2", "dummy2")
        notify(self.user1, "dummy3", "dummy3")
        GroupRequest.objects.create(
            status="pending", user=self.user3, group=self.group1
        )
        GroupRequest.objects.create(
            status="pending", user=self.user4, group=self.group1
        )
        StandingRequest.objects.create(
            user=self.user3,
            contact_id=1,
            contact_type_id=1,
        )
        StandingRequest.objects.create(
            user=self.user3,
            contact_id=2,
            contact_type_id=1,
        )
        s = StandingRequest.objects.create(
            user=self.user4,
            contact_id=4,
            contact_type_id=1,
        )
        s.action_by = self.user1
        s.save()

        StandingRevocation.objects.create(
            contact_id=4,
            contact_type_id=1,
        )
        p = Permission.objects.get(
            codename="affect_standings", content_type__app_label="standingsrequests"
        )
        self.user1.user_permissions.add(p)
        User.objects.get(pk=self.user1.pk)

        self.request = self.factory.get("/dashboard-addon/get_todo_counts")
        self.request.user = self.user1

    @patch("dashboard_alerts.views.get_config")
    def test_config_1(self, mock_get_config):
        config = self.DASHBOARD_ADD_ON_CONFIG
        config["notifications_enabled"] = False
        mock_get_config.return_value = config
        data = json.loads(get_todo_counts(self.request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 0)
        self.assertEqual(data["group_requests_count"], 2)
        self.assertEqual(data["standings_requests_count"], 3)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_config_2(self, mock_get_config):
        config = self.DASHBOARD_ADD_ON_CONFIG
        config["group_requests_enabled"] = False
        mock_get_config.return_value = config
        data = json.loads(get_todo_counts(self.request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 3)
        self.assertEqual(data["group_requests_count"], 0)
        self.assertEqual(data["standings_requests_count"], 3)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_config_3(self, mock_get_config):
        config = self.DASHBOARD_ADD_ON_CONFIG
        config["standings_requests_enabled"] = False
        mock_get_config.return_value = config
        data = json.loads(get_todo_counts(self.request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 3)
        self.assertEqual(data["group_requests_count"], 2)
        self.assertEqual(data["standings_requests_count"], 0)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_config_4(self, mock_get_config):
        config = self.DASHBOARD_ADD_ON_CONFIG
        config["disable_app_alerts_for_superusers"] = True
        mock_get_config.return_value = config
        self.user1.is_superuser = True
        self.user1.save()
        data = json.loads(get_todo_counts(self.request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 3)
        self.assertEqual(data["group_requests_count"], 2)
        self.assertEqual(data["standings_requests_count"], 0)
        self.assertEqual(data["alert_level"], "info")

    @patch("dashboard_alerts.views.get_config")
    def test_config_5(self, mock_get_config):
        config = self.DASHBOARD_ADD_ON_CONFIG
        config["danger_threshold"] = 3
        config["warning_threshold"] = 2
        mock_get_config.return_value = config
        data = json.loads(get_todo_counts(self.request).content.decode("utf-8"))
        self.assertTrue(data["has_to_dos"])
        self.assertEqual(data["notifications_count"], 3)
        self.assertEqual(data["group_requests_count"], 2)
        self.assertEqual(data["standings_requests_count"], 3)
        self.assertEqual(data["alert_level"], "danger")
