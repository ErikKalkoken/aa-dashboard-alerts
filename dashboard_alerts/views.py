import logging

from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.urls import reverse

from allianceauth.notifications.models import Notification
from allianceauth.groupmanagement.models import GroupRequest

from .config import get_config


logger = logging.getLogger(__name__)


def _app_labels() -> set:
    """returns set of all current app labels"""
    return {x for x in apps.app_configs.keys()}


@login_required
def get_todo_counts(request):
    """returns todo counts for current user"""

    config = get_config()

    # count notification requests
    if config["notifications_enabled"]:
        notifications_count = (
            Notification.objects.filter(user=request.user).filter(viewed=False).count()
        )
    else:
        notifications_count = 0

    # count group requests
    if config["group_requests_enabled"]:
        group_requests_count = (
            GroupRequest.objects.filter(status="pending")
            .filter(group__authgroup__group_leaders__exact=request.user)
            .count()
        )
    else:
        group_requests_count = 0

    # count standings requests
    standings_requests_count = 0
    standings_requests_link = None
    if (
        config["standings_requests_enabled"]
        and (
            not request.user.is_superuser
            or not config["disable_app_alerts_for_superusers"]
        )
        and "standingsrequests" in _app_labels()
    ):
        if request.user.has_perm("standingsrequests.affect_standings"):
            standings_requests_link = reverse("standingsrequests:manage")
            from standingsrequests.models import StandingRequest
            from standingsrequests.models import StandingRevocation

            standings_requests_count = (
                StandingRequest.objects.filter(action_by__exact=None)
                .filter(is_effective__exact=False)
                .count()
            )
            standings_requests_count += (
                StandingRevocation.objects.filter(action_by__exact=None)
                .filter(is_effective__exact=False)
                .count()
            )

    # count SRP requests
    srp_requests_count = 0
    srp_requests_link = None
    if (
        config["srp_requests_enabled"]
        and (
            not request.user.is_superuser
            or not config["disable_app_alerts_for_superusers"]
        )
        and "srp" in _app_labels()
    ):
        if request.user.has_perm("auth.srp_management"):
            srp_requests_link = reverse("srp:management")
            from allianceauth.srp.models import SrpUserRequest

            srp_requests_count = SrpUserRequest.objects.filter(
                srp_status="pending"
            ).count()

    # determine alert level
    total_count = (
        notifications_count
        + group_requests_count
        + standings_requests_count
        + srp_requests_count
    )

    if total_count >= config["danger_threshold"]:
        alert_level = "danger"
    elif total_count >= config["warning_threshold"]:
        alert_level = "warning"
    else:
        alert_level = "info"

    data = {
        "has_to_dos": total_count > 0,
        "notifications_count": notifications_count,
        "notifications_link": reverse("notifications:list"),
        "group_requests_count": group_requests_count,
        "group_requests_link": reverse("groupmanagement:management"),
        "standings_requests_count": standings_requests_count,
        "standings_requests_link": standings_requests_link,
        "srp_requests_count": srp_requests_count,
        "srp_requests_link": srp_requests_link,
        "alert_level": alert_level,
    }
    return JsonResponse(data)
