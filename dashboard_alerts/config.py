from django.conf import settings

# defaults
DASHBOARD_ADD_ON_CONFIG = {
    "notifications_enabled": True,
    "group_requests_enabled": True,
    "standings_requests_enabled": True,
    "srp_requests_enabled": True,
    "warning_threshold": 10,
    "danger_threshold": 25,
    "disable_app_alerts_for_superusers": False,
}


def get_config() -> dict:
    """returns the current config"""
    config = DASHBOARD_ADD_ON_CONFIG

    # override with dict from settings if it exists
    try:
        for k, v in settings.DASHBOARD_ADD_ON_CONFIG.items():
            config[k] = v
    except AttributeError:
        # config not defined in settings
        pass

    return config
