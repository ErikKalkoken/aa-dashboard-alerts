from django.apps import AppConfig


class DashboardAlertsConfig(AppConfig):
    name = "dashboard_alerts"
    label = "dashboard_alerts"
