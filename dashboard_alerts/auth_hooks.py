from allianceauth.services.hooks import UrlHook
from allianceauth import hooks
from . import urls


@hooks.register("url_hook")
def register_urls():
    return UrlHook(urls, "dashboard_alerts", r"^dashboard_alerts/")
