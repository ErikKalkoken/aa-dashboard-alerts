# Dashboard Alerts

This is a plugin for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth) that adds alerts for open items to the dashboard.

## Overview

This app adds an alert message to the dashboard to notify users about their current open items like pending group requests and unread notifications.

Here is an example screenshot:

![screenshot](https://i.imgur.com/Y0jn7I6.png)

## Features

- Shows an alert message on the dashboard informing the user about his:
  - unread notifications
  - pending group requests (for group leaders only)
  - pending standings requests (for standings managers only) *)
  - pending SRP requests (for SRP managers only) *)

- Alerts are shown with info, warning and danger formatting based on the amount of open items

- Thresholds for alert levels and for which apps alerts are shown can be configured

*) This feature is only enabled if the respective plugin app is installed

## Installation

### 1. Install app

Install this app simply by running pip install from this repo. Make sure you are in your AA virtual environment.

```bash
pip install git+https://gitlab.com/ErikKalkoken/aa-dashboard-alerts.git
```

### 2. Update AA settings

Add this app to `INSTALLED_APPS` in your AA settings. The app's name is `dashboard_alerts`

(No migrations required)

### 3. Add or update the custom dashboard template

Add the provided dashboard template to overload the default one or or manually add the two modifications if you already have a custom version of this template.

>**Note**: We are assuming that your AA project is called `myauth` as this is the name used in the official installation guide. Make sure to adjust the paths in this section accordingly if you have a different name.

#### Using the provided copy from the repo

You find the provide copy in the repo at: `/myauth/templates/authentication/dashboard.html`

To add the provided copy to your AA installation execute the following command from your myauth root directory (the ony that contains manage.py).

```bash
wget -P myauth/templates/authentication https://gitlab.com/ErikKalkoken/aa-dashboard-alerts/raw/master/myauth/templates/authentication/dashboard.html
```

If you are doing this with root you also want to make your AA user the owner of the new folders.

```bash
chown -R allianceserver:allianceserver myauth/templates
```

#### Alternatively: Update your custom template

If you already have a custom version of the dashboard template change it by manually adding the two modifications you find in the provided version in this repo. You can identify the modifications by comments like these:

```html
<!-- dashboard_alerts mod x start -->
...
<!-- dashboard_alerts mod x end -->
```

### 4. Configuration (optional)

You can configure this app to make fit better any special requirements.

See section **Configuration** below for a list of all configuration options.

### 5. Restart AA

Finally restart your AA instance by restarting your supervisors to activate the new app.

## Update

To install an update just re-run pip install from this repo. Make sure you are in your AA virtual environment.

```bash
pip install git+https://gitlab.com/ErikKalkoken/aa-dashboard-alerts.git
```

## Configuration

This app comes with a default configuration out of the box and does not require any additional configuration. However, it is possible to configure many features in order to customize the app for your individual requirements.

To adjust any of the defaults of the app add your custom configuration in a dictionary names `DASHBOARD_ADD_ON_CONFIG` to your local settings file.

Example:

```python
DASHBOARD_ADD_ON_CONFIG = {
    'notifications_enabled': False
}
```

Here is a list of all available settings:

Name | Default | Description
-- | -- | --
notifications_enabled | True | Enables alerts for unread notifications
group_requests_enabled | True | Enables alerts for pending group requests
standings_requests_enabled | True | Enables alerts for pending standings requests
srp_requests_enabled | True | Enables alerts for pending SRP requests
disable_app_alerts_for_superusers | False | Disable all alerts except notifications and group requests for superusers
warning_threshold | 10 | start showing alerts with warning level
danger_threshold | 25 | start showing alerts with danger level
